const canvas = document.getElementById("canvas");
const ctx = canvas.getContext('2d');
canvas.width = window.innerWidth * 0.6;
canvas.height = window.innerHeight * 0.75;

/*pencil starter settings*/
currentColor = "#43a5ad";
ctx.strokeStyle = currentColor;
ctx.lineJoin = 'round';
ctx.lineCap = 'round';
ctx.lineWidth = 10;

// ctx.setLineDash([1, 70]); <i class="fas fa-ellipsis-h"></i> <i class="fas fa-ellipsis-v"></i> <- KROPKOWA IKONA ! KROPKOWA LINIA! XD
// ctx.lineDashOffset = 50;
let isDrawing = false;
let x = 0;
let y = 0;

/*drawing on canvas function*/
function draw(e) {
        if (!isDrawing) return;
        ctx.beginPath();
        ctx.moveTo(x, y); // canvas coordinates of paint endpoints
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.stroke();
        ctx.closePath(x, y);
        x = e.offsetX;
        y = e.offsetY;
        console.log(`x: ${e.offsetX}, y: ${e.offsetY}`);
    }

canvas.addEventListener('mousemove', draw);
canvas.addEventListener('mousedown', (e) => {
    isDrawing = true;
    [x, y] = [e.offsetX, e.offsetY];
});
canvas.addEventListener('mouseup', () => isDrawing = false);
canvas.addEventListener('mouseout', () => isDrawing = false);

/*BUTTONS*/
/*PICKING COLOR*/
document.getElementById("colorButton").addEventListener("change", function () { /* TO FIX!!!!!!! - doesn't work after erasing */
    var currentColor = document.getElementById("color_value").value;
    ctx.strokeStyle = `#${currentColor}`;
});

/*ERASER*/
document.getElementById("eraserButton").addEventListener("click", function () {
    ctx.globalCompositeOperation="destination-out";
    ctx.fill();
});

/*UNDO*/
/*????? get array and push moves into?????*/

/*ERASING WHOLE CANVAS*/
document.getElementById('clear').addEventListener('click', () => {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}, false);

/*CHANGING PEN SIZE*/
document.getElementById("sizeButton").addEventListener("click", function () {
    document.getElementById("divRange").style.display = "block"; 
    var currentSize = document.getElementById("sizeRange").value;
    ctx.lineWidth = `${currentSize}`;
})